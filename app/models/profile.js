var mongoose = require('mongoose');

var userSchema = new mongoose.Schema({
	user_id : String,
	firstname : String,
	lastname : String,
	age : String,
	gender : String,
	created_date : String,
	signup_time : String,
});



var profile = mongoose.model( 'profile', userSchema );


module.exports = profile;
