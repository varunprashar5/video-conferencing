var mongoose = require('mongoose');

var userSchema = new mongoose.Schema({
	id : String,
	email : String,
	password : String,
	type : String,
	token : String,
	ip_address : String,
	latitude : String,
	longitude : String
});



var user = mongoose.model( 'user', userSchema );


module.exports = user;
