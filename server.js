// set up configruations //
var express  = require('express');
var app      = express(); 								// create our app w/ express
var mongoose = require('mongoose'); 					// mongoose for mongodb
var port  	 = process.env.PORT || 8080; 				// set the port
var database = require('./config/database'); 			// load the database config

var morgan = require('morgan'); 		// log requests to the console (express4)
var bodyParser = require('body-parser'); 	// pull information from HTML POST (express4)
var methodOverride = require('method-override'); // simulate DELETE and PUT (express4)
var jwt = require('jwt-simple');
var path = require('path');
var qs = require('querystring');
var async = require('async');
var bcrypt = require('bcryptjs');
var moment = require('moment');
var request = require('request');

var config = require('./config/config');

// database connection //
mongoose.connect(database.url); 	// connect to mongoDB database on modulus.io
var User = require('./app/models/account');
var User = require('./app/models/profile');
// listen (start app with node server.js) //
app.listen(port);
console.log("App listening on port " + port);
